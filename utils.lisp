;;;; Utility functions

(in-package :whatscl)

(defmacro cassoc (key alist)
  "Macro for (CDR (ASSOC KEY ALIST))."
  `(cdr (assoc ,key ,alist)))

(defmacro aval (key alist)
  "Retrieves the value associated with KEY from the association list ALIST, throwing an error if the value wasn't found."
  (let ((val-sym (gensym))
        (key-sym (gensym)))
    `(let* ((,key-sym ,key)
            (,val-sym (cassoc ,key-sym ,alist)))
       (unless ,val-sym
         (error "Malformed WhatsApp response: missing ~A" ,key-sym))
       ,val-sym)))

(defmacro map-when (func value)
  "If VALUE is not NIL, applies FUNC to VALUE and returns the result."
  (let ((val-sym (gensym)))
    `(let ((,val-sym ,value))
       (when ,val-sym
         (funcall ,func ,val-sym)))))

(defun split-at (seq pos &optional (consume-value-at-pos-p t))
  "Splits SEQ at POS, returning two values. If CONSUME-VALUE-AT-POS-P is truthy, throws away the element at POS."
  (declare (type sequence seq) (type fixnum pos))
  (cond
    ((> pos (length seq)) (error "Position is outside sequence."))
    ((eql pos (length seq)) (values seq nil))
    (t (values (subseq seq 0 pos) (subseq seq (+ pos (if consume-value-at-pos-p 1 0)))))))
