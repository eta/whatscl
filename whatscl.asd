(defsystem "whatscl"
  :depends-on ("websocket-driver-client" "ironclad" "cl-qrencode" "qbase64" "cl-json" "babel" "alexandria" "split-sequence" "nibbles" "flexi-streams" "bordeaux-threads" "com.google.base" "protobuf" "trivial-timers" "local-time" "trivial-backtrace")
  :serial t
  :components
  ((:file "packages")
   (:file "utf-16")
   (:file "utils")
   (:file "crypto")
   (:file "connection")
   (:file "binproto")
   (:file "message_wire")
   (:file "tables")
   (:file "message")
   (:file "api")
   (:file "errors")
   (:file "whatscl")))
