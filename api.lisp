;;;; Higher-level API types

(in-package :whatscl)

(defclass contact ()
  ((jid
    :accessor contact-jid
    :initarg :jid
    :initform (error "A contact JID must be provided")
    :documentation "JID (unique address) for this contact.")
   (notify
    :accessor contact-notify
    :initarg :notify
    :initform nil
    :documentation "The name the user set for themselves on WhatsApp (i.e. the ~name).")
   (name
    :accessor contact-name
    :initarg :name
    :initform nil
    :documentation "A name for this contact, as specified in the user's address book."))
  (:documentation "An entry in the list of the user's WhatsApp contacts."))

(defun parse-contact (contact)
  "Parse the wire-formatted CONTACT into an actual `contact' object."
  (make-instance 'contact
                 :jid (aval :jid contact)
                 :notify (cassoc :notify contact)
                 :name (cassoc :name contact)))

(defmethod print-object ((obj contact) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (jid notify name) obj
      (format stream "~A~@[ (\"~~~A\")~]~@[ (\"~A\")~]" jid notify name))))

(defclass chat-entry ()
  ((jid
    :accessor chat-jid
    :initarg :jid
    :initform (error "A groupchat JID must be provided")
    :documentation "JID (unique address) for this chat entry.")
   (subject
    :accessor chat-subject
    :initarg :subject
    :initform nil
    :documentation "If this chat is a groupchat, the subject (topic) of the groupchat.")
   (last-activity
    :accessor chat-last-activity
    :initarg :last-activity
    :initform nil
    :documentation "The Unix timestamp of the last thing which happened in this chat.")
   (is-spam
    :accessor chat-is-spam
    :initarg :is-spam
    :initform nil
    :documentation "Whether the chat is marked as spam.")
   (is-read-only
    :accessor chat-is-read-only
    :initarg :is-read-only
    :initform nil
    :documentation "Whether the chat is read-only (you can't send to it)")
   (muted-until
    :accessor chat-muted-until
    :initarg :muted-until
    :initform nil
    :documentation "The Unix timestamp of when this chat is muted until, or NIL if the chat is not muted.")
   (modify-tag
    :accessor chat-modify-tag
    :initarg :modify-tag
    :initform nil))
  (:documentation "An entry in the user's list of WhatsApp chats (i.e. private messages and groupchats)."))

(defun parse-chat-entry (entry)
  "Parse the wire-formatted ENTRY into an actual `chat-entry' object."
  (flet ((is-true (x) (equal x "true"))
         (parse-muted (x)
           (let ((ts (parse-integer x)))
             (unless (eql ts 0) ts))))
    (make-instance 'chat-entry
                   :jid (aval :jid entry)
                   :subject (cassoc :name entry)
                   :last-activity (parse-integer (aval :t entry))
                   :is-spam (map-when #'is-true (cassoc :spam entry))
                   :is-read-only (map-when #'is-true (cassoc :read_only entry))
                   :muted-until (map-when #'parse-muted (cassoc :mute entry))
                   :modify-tag (cassoc :modify_tag entry))))

(defmethod print-object ((obj chat-entry) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (jid subject) obj
      (format stream "~A~@[ (\"~A\")~]" jid subject))))

(defun parse-pb-object (vector class length)
  "Parse a protobuf object of class CLASS from the bytes stored in VECTOR."
  (let ((ret (make-instance class)))
    (pb:merge-from-array ret vector 0 length)
    ret))

(defun parse-web-message (vector)
  "Parse a protobuf chat message stored in VECTOR."
  (parse-pb-object vector 'wpb:web-message-info (length vector)))
