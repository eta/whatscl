;;;; WhatsApp Web client for Common Lisp!

(in-package :whatscl)

(defparameter *current-web-version* '(2 2142 12)
  "The current WhatsApp Web client version.")
(defparameter *default-long-desc*
  (concatenate 'string "whatscl on " (lisp-implementation-type) " " (lisp-implementation-version))
  "The default long browser description to use (displayed in the list of web sessions in-app)")
(defparameter *default-short-desc* "whatscl"
  "The default short browser description to use (used for some unknown purpose).")
(defparameter *websocket-url* "wss://web.whatsapp.com/ws"
  "The WebSocket URL to connect to for WhatsApp Web.")
(defparameter *default-client-version* "0.1.0"
  "The client version to provide to WhatsApp Web.")
(defparameter *websocket-headers*
  '(("Origin" . "https://web.whatsapp.com"))
  "Headers to use when connecting to the WebSocket.")
(defparameter *ping-interval-secs* 13
  "Seconds to wait between sending pings.")
(defparameter *ping-deadline-secs* 3
  "Seconds to wait for a pong before declaring a connection timed out.")
(defvar *whatscl-null-stream* (make-broadcast-stream))
(defvar *whatscl-debug-stream* *whatscl-null-stream*)
(defvar *debug-out* (make-synonym-stream '*whatscl-debug-stream*))

(defvar *last-message-tag* 0)

(defmacro debug-format (&rest args)
  "Calls (FORMAT *DEBUG-OUT* ,@ARGS), but only if *whatscl-debug-stream* is set to something useful. This optimization prevents us from spinning CPU trying to format yuuuge WhatsApp messages."
  `(unless (eql *whatscl-debug-stream* *whatscl-null-stream*)
     (format *debug-out* ,@args)))

(defmacro without-wac-lock ((wac) &body forms)
  "The opposite of WITH-WAC-LOCK: evaluates FORMS after releasing the connection lock, and then re-acquires it afterwards."
  (let ((lock (gensym))
        (wac-sym (gensym)))
    `(let* ((,wac-sym ,wac)
            (,lock (wac-lock ,wac-sym)))
       (bt:release-lock ,lock)
       (unwind-protect
            (progn
              ,@forms)
         (bt:acquire-lock ,lock)))))

(defmacro wac-emit (event wac &rest args)
  "Calls EMIT on WAC, but makes sure to release the connection lock before doing so and re-acquire it afterwards."
  (let ((lock (gensym))
        (wac-sym (gensym)))
    `(let* ((,wac-sym ,wac)
            (,lock (wac-lock ,wac-sym)))
       (bt:release-lock ,lock)
       (unwind-protect
            (emit ,event ,wac-sym ,@args)
         (bt:acquire-lock ,lock)))))

(defun make-message-tag ()
  "Generate a new unique message tag."
  (format nil "t~A-~A" (get-universal-time) (incf *last-message-tag*)))

(defun make-client-id ()
  "Generate a WhatsApp Web client ID."
  (qbase64:encode-bytes (crypto:random-data 16)))

(defun build-login-message (client-id &optional (long-desc *default-long-desc*) (short-desc *default-short-desc*) (version *current-web-version*) (client-version *default-client-version*))
  "Returns a login message: the first message sent down the websocket."
  `("admin" "init" ,version (,long-desc ,short-desc ,client-version) ,client-id t))

(defun build-restore-message (session)
  "Returns a message used to restore a disconnected session."
  `("admin" "login" ,(session-client-token session) ,(session-server-token session) ,(session-client-id session) "takeover"))

(defun parse-string-ws-message (msg)
  "Parse a string websocket message, returning (tag . json-data) if successful, :pong if the message was a pong message, or NIL if the message was invalid."
  (let ((comma (position #\, msg)))
    (if comma
        (multiple-value-bind (tag payload)
            (split-at msg comma)
          (cons tag (when (and payload (> (length payload) 0))
                      (cl-json:decode-json-from-string payload))))
        (when (starts-with #\! msg)
          :pong))))

(defun parse-binary-ws-message (conn msg)
  "Parse, validate and decrypt a binary websocket message, returning (tag . data) if successful and NIL if not."
  (let ((comma (position (char-code #\,) msg)))
    (when comma
      (multiple-value-bind (tag payload)
          (split-at msg comma)
        (cons (babel:octets-to-string tag)
              (deserialize-binary-message
               (validate-and-decrypt-binary-ws-message
                (wac-session conn) payload)))))))

(defun parse-ws-message (conn msg)
  "Parse a message received through the websocket."
  (etypecase msg
    ((vector (unsigned-byte 8)) (parse-binary-ws-message conn msg))
    (string (parse-string-ws-message msg))))

(defun on-ping-timeout (conn)
  "Called after a ping timeout happens."
  (with-wac-lock (conn)
    (wac-emit :ping-timeout conn)
    (wsd:close-connection (wac-ws conn))))

(defun on-ping-timer (conn)
  "Callback for the periodic ping timer."
  (with-wac-lock (conn)
    (when (wac-is-connected conn)
      (wsd:send-text (wac-ws conn) "?,,")
      (trivial-timers:schedule-timer (wac-response-timer conn) *ping-deadline-secs*)
      (trivial-timers:schedule-timer (wac-ping-timer conn) *ping-interval-secs*))))

(defun send-ws-message (conn message &optional callback)
  "Send a message through the websocket of connection CONN. MESSAGE can be a string, byte vector, or other JSON-encodable object (and the appropriate type of message will be sent, depending on its type).
CALLBACK, if provided, specifies a function to run with the reply sent by WhatsApp, if one is received."
  (with-wac-lock (conn)
    (with-accessors ((ws wac-ws) (outgoing wac-outgoing)) conn
      (let ((tag (make-message-tag)))
        (debug-format "<-- message (tag ~A): ~A~%" tag message)
        (typecase message
          (string (wsd:send-text ws (format nil "~A,~A" tag message)))
          ((vector (unsigned-byte 8))
           (wsd:send-binary ws (concatenate 'vector
                                            (babel:string-to-octets tag)
                                            `#(,(char-code #\,))
                                            message)))
          (t (wsd:send-text ws (format nil "~A,~A" tag (cl-json:encode-json-to-string message)))))
        (when callback
          (setf (gethash tag outgoing) callback))
        tag))))

(defun send-ws-node-message (conn node metric &key tag callback)
  "Send a binary NODE through the websocket of connection CONN, using the supplied message TAG if one is provided and the message metric code METRIC.
Warning: modifies NODE.
CALLBACK, if provided, specifies a function to run with the reply sent by WhatsApp, if one is received."
  (with-wac-lock (conn)
    (with-accessors ((ws wac-ws) (sess wac-session) (outgoing wac-outgoing) (epoch wac-epoch)) conn
      (incf epoch)
      (setf (second node) (acons :epoch (write-to-string epoch) (second node)))
      (unless (and sess (session-mac-key sess))
        (push (list node metric tag callback) (wac-outgoing-queue conn))
        (warn "Tried to send node message ~A before a session was established." node)
        (return-from send-ws-node-message))
      (let* ((tag (or tag (make-message-tag)))
             (metric (unsymbolize-message-metric metric))
             (serialized (serialize-binary-message node))
             (encrypted (sign-and-encrypt-binary-ws-message sess serialized))
             (metric-bit `#(,(char-code #\,) ,metric #x80))
             (payload (concatenate '(vector (unsigned-byte 8))
                                   (babel:string-to-octets tag)
                                   metric-bit encrypted)))
        (debug-format "~&<-- binary node (tag ~A, epoch ~A, len ~A):~%~&    ~A~%~&~A~%" tag epoch (length payload) node payload)
        (wsd:send-binary ws payload)
        (when callback
          (setf (gethash tag outgoing) callback))))))

(defun send-message (conn message &optional callback)
  "Send a WPB:WEB-MESSAGE-INFO object MESSAGE. Returns the message's ID. The CALLBACK will be called with CONN as first argument and the server's response: an alist, which should on success contain the key :STATUS with value 200."
  (check-type message wpb:web-message-info)
  (let ((msgid (pb:string-value (wpb:id (wpb:key message)))))
    (send-ws-node-message conn `(:action ((:type . :relay)) ((:message () ,(encode-message message)))) :message
                          :tag msgid
                          :callback (lambda (conn result)
                                      (without-wac-lock (conn)
                                        (funcall callback conn result))))
    msgid))

(defun send-simple-text-message (conn to-jid text &optional callback)
  "Send a simple text message to the JID TO-JID, containing TEXT. Returns the message's ID. The CALLBACK will be called with CONN as first argument and the server's response: an alist, which should on success contain the key :STATUS with value 200."
  (check-type to-jid jid)
  (check-type text string)
  (send-message conn (make-simple-text-message to-jid text) callback))


(defun send-simple-image-message (conn to-jid image-contents &optional callback)
  "Send a simple image message to the JID TO-JID, containing IMAGE-CONTENTS (a MESSAGE-CONTENTS-IMAGE object). Returns the message's ID. The CALLBACK will be called with CONN as first argument and the server's response: an alist, which should on success contain the key :STATUS with value 200."
  (check-type to-jid jid)
  (check-type image-contents message-contents-image)
  (send-message conn (make-image-message to-jid image-contents) callback))


(defun send-message-read (conn orig-from msgid)
  "Mark the message originally sent by ORIG-FROM (a JID), with WhatsApp message ID MSGID, as being read."
  (declare (type jid orig-from) (type string msgid))
  (send-ws-node-message conn
                        `(:action ((:type . :set))
                                  ((:read ((:index . ,msgid)
                                           (:jid . ,orig-from)
                                           (:owner . :false)
                                           (:count . "1"))
                                          nil)))
                        :read
                        :callback (function-check-status :send-message-read)))

(defun send-presence (conn presence-type &optional jid)
  "Send the presence PRESENCE-TYPE on CONN, with an optional JID string if the presence indicates composing, recording, or paused composition."
  (declare (type (member :available :unavailable :composing :recording :paused) presence-type)
           (type (or jid null) jid))
  (send-ws-node-message conn
                        `(:action ((:type . :set))
                                  ((:presence ((:type . ,presence-type)
                                               ,@(when jid
                                                   `((:to . ,(jid-to-message-target jid)))))
                                              nil)))
                        :presence
                        :callback (function-check-status :send-presence)))

(defun parse-media-conn-response (resp)
  "Parse the response to START-MEDIA-UPLOAD provided (RESP). If it is valid, return the values (AUTH-TOKEN TTL HOSTNAME-LIST); else, return NIL."
  (labels
      ;; This is the kind of thing the Maybe monad is excellent at. We don't have
      ;; that, so let's do things the hacky Lisp way!
      ((need (thing) (or thing (return-from parse-media-conn-response)))
       (maybe-list (obj) (when obj (list obj)))
       (extract-hostname (host) (maybe-list (cassoc :hostname host))))
    (let* ((media-conn (need (cassoc :media--conn resp)))
           (auth-token (need (cassoc :auth media-conn)))
           (ttl (need (cassoc :ttl media-conn)))
           (hosts (mapcan #'extract-hostname (need (cassoc :hosts media-conn)))))
      (values auth-token ttl hosts))))

(defun start-media-upload (conn callback)
  "Request a media upload slot from WhatsApp. Calls CALLBACK like (FUNCALL CALLBACK CONN AUTH-TOKEN TTL HOSTS); if the slot request fails, AUTH-TOKEN, TTL, and HOSTS will all be NIL."
  (send-ws-message conn `("query" "mediaConn")
                   (lambda (conn resp)
                     (multiple-value-bind (auth-token ttl hosts)
                         (parse-media-conn-response resp)
                       (without-wac-lock (conn)
                         (funcall callback conn auth-token ttl hosts))))))

(defun get-profile-picture (conn jid callback)
  "Try and get a profile picture thumbnail for JID. Calls CALLBACK with CONN as first argument and then a URL (or NIL if none could be found)"
  (declare (type jid jid))
  (send-ws-message conn `("query" "ProfilePicThumb" ,(jid-to-string jid))
                   (lambda (conn result)
                     (without-wac-lock (conn)
                       (funcall callback conn (when result
                                                (cassoc :eurl result)))))))

(defun replace-surrogates (str)
  (substitute-if #\uFFFD (lambda (ch) (<= #xD800 (char-code ch) #xDFFF)) str))

(defun fix-cl-json-string (string)
  "Fixes the STRING returned by CL-JSON, properly decoding any UTF-16 surrogate pairs instead of passing them through as invalid characters.
FIXME: We should ideally just use a better JSON library..."
  (replace-surrogates (map 'string #'code-char (whatscl/utf16-hacks::decode-utf-16 (map 'vector #'char-code string)))))

#-cl-json-already-patched
(progn
  ;; FIXME: Monkey patch CL-JSON to be less atrocious at UTF-16.
  (defun cl-json::string-stream-accumulator-get ()
    "Return all characters accumulated so far in a string-stream
accumulator and close the stream."
    (whatscl::fix-cl-json-string
     (prog1 (get-output-stream-string cl-json::*accumulator*)
       (close cl-json::*accumulator*))))
  ;; Needed to get it to take the new function
  (cl-json:set-decoder-simple-list-semantics)
  (push :cl-json-already-patched *features*))

(defun get-profile-status (conn jid callback)
  "Try and get the status text set by JID. Calls CALLBACK with CONN as first argument and then the status text (or NIL if none could be found)"
  (declare (type jid jid))
  (send-ws-message conn `("query" "Status" ,(jid-to-string jid))
                   (lambda (conn result)
                     (without-wac-lock (conn)
                       (funcall callback conn (when result
                                                (cassoc :status result)))))))

(defun get-group-metadata (conn jid callback)
  "Try and get group metadata for the group identified by JID. Calls CALLBACK with CONN as first argument and then the group metadata (or NIL if none could be found)."
  (declare (type jid jid))
  (send-ws-message conn `("query" "GroupMetadata" ,(jid-to-string jid))
                   (lambda (conn result)
                     (without-wac-lock (conn)
                       (funcall callback conn result)))))

(defun process-chat-history-payload (payload)
  "Processes a returned chat history payload, returning a list of messages."
  (loop
    for (tag attrs msgdata) in (third payload)
    append (handler-case
               (list (parse-message (parse-web-message msgdata)))
             (error (e) (warn "Failed to process message as part of chat history: ~A" e)))))

(defun get-chat-history (conn jid callback &key before (owner t) (chunk-size 50))
  (check-type jid jid)
  (send-ws-node-message conn `(:query ((:type . :message)
                                       (:jid . ,(jid-to-message-target jid))
                                       ,@(when before
                                           `((:index . ,before)))
                                       (:owner . ,(if owner
                                                      "true"
                                                      "false"))
                                       (:count . ,(princ-to-string chunk-size)))
                                      ())
                        :group
                        :callback (lambda (conn result)
                                    (without-wac-lock (conn)
                                      (funcall callback conn (process-chat-history-payload result))))))

(defun get-full-chat-history (conn jid user-callback &key (sleep-secs 0.1) chunk-size)
  "Retrieve full chat history for the WhatsApp JID on CONN, calling USER-CALLBACK with a list of all history messages for that chat when done. Waits SLEEP-SECS seconds between making requests for history of size CHUNK-SIZE."
  (check-type jid jid)
  (bt:make-thread
   (lambda ()
     (handler-case
         (let* ((ret)
                (before)
                (owner t))
           (labels ((callback (conn result)
                      (bt:make-thread
                       (lambda ()
                         (if result
                             (progn
                               (setf ret (nconc result ret))
                               (setf before (whatscl::message-id (elt ret 0)))
                               (setf owner (typep (whatscl::message-key (elt ret 0)) 'message-key-sending))
                               (sleep sleep-secs)
                               (debug-format "~&chat history: querying before=~A owner=~A~%" before owner)
                               (get-chat-history conn jid #'callback
                                                 :before before
                                                 :owner owner
                                                 :chunk-size chunk-size))
                             (progn
                               (debug-format "~&done! result length ~A~%" (length ret))
                               (funcall user-callback conn ret))))
                       :name "whatscl chat history thunk")))
             (get-chat-history conn jid #'callback
                               :chunk-size chunk-size)))
       (error (e)
         (warn "Full chat history request failed: ~A" e)
         (funcall user-callback conn nil))))
   :name "whatscl chat history thread"))

(defun on-login-message (conn details)
  "Called when WhatsApp sends us the initial login information."
  (with-accessors ((keypair wac-keypair) (client-id wac-client-id)) conn
    (unless (eql (aval :status details) 200)
      (error 'login-error
             :operation :on-login-message
             :status-code (aval :status details)))
    (setf keypair (multiple-value-list (crypto:generate-key-pair :curve25519)))
    (let* ((pubkey
             (cadr (crypto:destructure-public-key (cadr keypair))))
           (qr-text
             (concatenate 'string (aval :ref details) ","
                          (qbase64:encode-bytes pubkey) ","
                          client-id)))
      (debug-format "QR code obtained from server~%")
      (wac-emit :qrcode conn qr-text))))

(defun handle-challenge (conn payload)
  "Handles the encryption key challenge that the server sends when restoring an old session."
  (let* ((sess (wac-session conn))
         (challenge (coerce (qbase64:decode-string (aval :challenge payload)) 'ironclad::simple-octet-vector))
         (signed-data (hmac-sha256 (session-mac-key sess) challenge))
         (response (qbase64:encode-bytes signed-data)))
    (debug-format "Handling server challenge~%")
    (send-ws-message conn `("admin" "challenge" ,response ,(session-server-token sess) ,(session-client-id sess))
                     (function-check-status :login-challenge 'login-error))))

(defun send-login-message (conn)
  "Sends the login message, using information in the client's LOGIN-INFORMATION if provided."
  (with-accessors ((client-id wac-client-id) (login-information wac-login-information)) conn
    (let ((login-msg (apply #'build-login-message client-id login-information))
          (callback (if (wac-session conn)
                        (lambda (conn resp)
                          (cb-check-status conn resp :on-login-message 'login-error)
                          (send-restore-message conn))
                        #'on-login-message)))
      (send-ws-message conn login-msg callback))))

(defun send-restore-message (conn)
  "Sends a session restore message, using the stored session in CONN."
  (let ((restore-msg (build-restore-message (wac-session conn))))
    (send-ws-message conn restore-msg
                     (function-check-status :restore-message 'login-error))))

(defun on-connection-ack (conn msg)
  "Handles a 'connection acknowledgement' message, performing key generation."
  (with-accessors ((session wac-session) (jid wac-jid) (keypair wac-keypair) (client-id wac-client-id) (outqueue wac-outgoing-queue)) conn
    (wac-emit :raw-connection-ack conn msg)
    (unless jid
      (unless session
        (multiple-value-bind (enc-key mac-key)
            (keys-from-secret (aval :secret msg) keypair)
          (setf session (make-instance 'persistent-session
                                       :client-token (aval :client-token msg)
                                       :server-token (aval :server-token msg)
                                       :client-id client-id
                                       :enc-key enc-key
                                       :mac-key mac-key))))
      (setf jid (aval :wid msg))
      (debug-format "Connected to WhatsApp as ~A.~%" jid)
      (wac-emit :connected conn jid)
      (when (> (length outqueue) 0)
        (warn "Sending ~A queued node messages" (length outqueue))
        (loop
          for (node metric tag callback) in outqueue
          do (send-ws-node-message conn node metric
                                   :tag tag
                                   :callback callback))
        (setf outqueue nil)))))

(defun on-contacts-payload (conn payload)
  "Handles a wire-format PAYLOAD of contacts information for CONN."
  (let ((contacts nil))
    (loop
      for entry in payload
      when (eql (car entry) :user)
        do (setf contacts (cons (parse-contact (second entry)) contacts)))
    (debug-format "Got contacts: ~A~%" contacts)
    (wac-emit :contacts conn contacts)))

(defun on-chats-payload (conn payload)
  "Handles a wire-format PAYLOAD of chats information for CONN."
  (let ((chats nil))
    (loop
      for entry in payload
      when (eql (car entry) :chat)
        do (setf chats (cons (parse-chat-entry (second entry)) chats)))
    (debug-format "Got chats: ~A~%" chats)
    (wac-emit :chats conn chats)))

(defun on-server-action (conn act attrs)
  "Handles a 'server action' message ACT."
  (let ((type (first act)))
    (cond
      ((eql type :message)
       (progn
         (let ((delivery-type (tentative-keywordize (aval :add attrs)))
               (msg
                 (handler-case
                     (parse-message (parse-web-message (third act)))
                   (error (e)
                     (warn "Failed to parse a message: ~A~%~A" e act)))))
           (when msg
             (debug-format "Got a message (delivered as ~A): ~A~%" delivery-type msg)
             (wac-emit :message conn msg delivery-type)))))
      ((eql type :user)
       (wac-emit :contact-add conn (parse-contact (second act))))
      (t (warn "Unknown server action: ~A" type)))))

(defun handle-picture-change (conn payload)
  "Handles a message about a contact changing their avatar."
  (let ((jid (parse-jid (aval :jid payload)))
        (was-removed (eql (cassoc :removed payload) t)))
    (wac-emit :picture-change conn jid was-removed)))

(defun handle-disconnect (conn payload)
  "Handles a disconnect message."
  (let ((kind (or
               (map-when #'tentative-keywordize (cassoc :kind payload))
               :removed)))
    (wac-emit :disconnect conn kind)
    (whatscl::close-connection conn)))

(defun on-server-presence (conn payload)
  "Handles a 'Presence' server message from the server."
  (wac-emit :presence conn
            :of (parse-jid (aval :id payload))
            :type (tentative-keywordize (aval :type payload))
            :participant (map-when #'parse-jid (cassoc :participant payload))
            :denied (cassoc :deny payload)))

(defun on-chat-modified (conn payload)
  "Handles a 'Chat' server message from the server."
  (let ((jid (parse-jid (aval :id payload))))
    ;; FIXME: Actually parse the payload.
    (wac-emit :chat-modified conn jid)))

(defun on-server-msg-message (conn payload)
  "Handles a 'Msg' server message from the server."
  (let ((cmd (aval :cmd payload)))
    (labels
        ((get-participant (payload)
           (let ((p (cassoc :participant payload)))
             (when p
               (parse-jid p)))))
      (cond
        ((equal cmd "ack")
         (wac-emit :message-ack conn
                   :id (aval :id payload)
                   :ack (symbolize-message-ack-level (aval :ack payload))
                   :from (parse-jid (aval :from payload))
                   :to (parse-jid (aval :to payload))
                   :participant (get-participant payload)
                   :ts (aval :t payload)))
        ((equal cmd "acks")
         (loop
           for id in (aval :id payload)
           do (wac-emit :message-ack conn
                        :part-of-many t
                        :id id
                        :ack (symbolize-message-ack-level (aval :ack payload))
                        :from (parse-jid (aval :from payload))
                        :to (parse-jid (aval :to payload))
                        :participant (get-participant payload)
                        :ts (aval :t payload))))
        (t
         (warn "Unknown 'Msg' server message type ~A" cmd))))))

(defun on-server-message (conn msg tag)
  "Handles a message from the server that isn't a reply to something we've sent."
  (if msg
      (destructuring-bind (opcode &rest payload) msg
        (cond
          ((equal opcode :response)
           (progn
             (let ((type (aval :type (first payload))))
               (cond
                 ((equal type "contacts") (on-contacts-payload conn (second payload)))
                 ((equal type "chat") (on-chats-payload conn (second payload)))
                 (t (warn "Invalid 'response' type ~A" type))))))
          ((equal opcode :action)
           (loop
             for node in (second payload)
             do (on-server-action conn node (first payload))))
          ((equal opcode "Conn") (on-connection-ack conn (car payload)))
          ((equal opcode "Msg") (on-server-msg-message conn (car payload)))
          ((equal opcode "Cmd")
           (progn
             (let* ((payload (car payload))
                    (type (aval :type payload)))
               (cond
                 ((equal type "challenge") (handle-challenge conn payload))
                 ((equal type "picture") (handle-picture-change conn payload))
                 ((equal type "disconnect") (handle-disconnect conn payload))
                 (t (warn "Unhandled server command type ~A: ~A" type msg))))))
          ((equal opcode "Props") (wac-emit :props conn (car payload)))
          ((equal opcode "MsgInfo") (on-server-msg-message conn (car payload)))
          ((equal opcode "Status") (wac-emit :status-change conn
                                             (parse-jid (aval :id (car payload)))
                                             (aval :status (car payload))))
          ((equal opcode "Call") (wac-emit :call conn (car payload)))
          ((equal opcode "Presence") (on-server-presence conn (car payload)))
          ((equal opcode "Chat") (on-chat-modified conn (car payload)))
          ((and (equal opcode "Stream") (equal (car payload) "asleep"))
           (wac-emit :stream-asleep conn))
          (t (warn "Unhandled server message: ~A" msg))))
      (if (> (length tag) 10)
          (progn
            (debug-format "Interpreting empty payload as an ack for ~A~%" tag)
            (wac-emit :message-pending-send conn tag))
          (debug-format "Empty server message~%"))))

(defun on-ws-open (conn)
  (with-wac-lock (conn)
    (debug-format "WebSocket connected.~%")
    (wac-emit :ws-connected conn)
    (setf (wac-is-connected conn) t)
    (send-login-message conn)
    (on-ping-timer conn)))

(defun on-ws-message (conn msg)
  "Handles an incoming message (binary or JSON) from the server."
  (with-wac-lock (conn)
    (with-accessors ((outgoing wac-outgoing) (reptimer wac-response-timer)) conn
      (let ((msg (parse-ws-message conn msg)))
        (wac-emit :raw-message conn msg)
        (if (eql msg :pong)
            (trivial-timers:unschedule-timer reptimer)
            (let ((last-err-backtrace))
              (destructuring-bind (tag &rest payload) msg
                (debug-format "--> message (tag ~A): ~S~%" tag payload)
                (handler-case
                    (handler-bind
                        ((error (lambda (e)
                                  (setf last-err-backtrace
                                        (trivial-backtrace:print-backtrace e
                                                                           :output nil)))))
                      (let ((callback (gethash tag outgoing)))
                        (if (and payload callback)
                            (progn
                              (remhash tag outgoing)
                              (funcall callback conn payload))
                            (on-server-message conn payload tag))))
                  (status-code-error (err)
                    (wac-emit :error-status-code conn err)
                    (when (typep err 'login-error)
                      ;; General STATUS-CODE-ERROR conditions aren't fatal,
                      ;; but login errors are.
                      (whatscl::close-connection conn)))
                  (error (err)
                    (warn "Error when handling message: ~A" err)
                    (warn "Backtrace: ~A" last-err-backtrace)
                    (wac-emit :error conn err last-err-backtrace)
                    (whatscl::close-connection conn))))))))))

(defun on-ws-error (conn err)
  (debug-format "WebSocket error: ~A~%" err)
  (with-wac-lock (conn)
    (wac-emit :ws-error conn err)))

#+wsd-loop-fixed
(defun mitigate-wsd-brokenness (conn)
  (declare (ignore conn)))

#-wsd-loop-fixed
(progn
  (warn "You're using a broken version of WEBSOCKET-DRIVER that might spin CPU on disconnection!")
  (defun mitigate-wsd-brokenness (conn)
    ;; WEBSOCKET-DRIVER brokenness: the read thread has crap
    ;; error handling and just gets stuck in a loop. We manually
    ;; kill it here to avoid it eating 100% CPU...
    (with-slots ((read-thread websocket-driver.ws.client::read-thread))
        (wac-ws conn)
      (when (and read-thread (bt:thread-alive-p read-thread)
                 (bt:destroy-thread read-thread))))))

(defun on-ws-close (conn &key reason code)
  (debug-format "WebSocket closed: ~A (~A)~%" reason code)
  (with-wac-lock (conn)
    (setf (wac-is-connected conn) nil)
    (setf (wac-jid conn) nil)
    (trivial-timers:unschedule-timer (wac-ping-timer conn))
    (trivial-timers:unschedule-timer (wac-response-timer conn))
    (wac-emit :ws-close conn reason code)
    (mitigate-wsd-brokenness conn)))

(defun bind-ws-events (conn)
  (let ((ws (wac-ws conn)))
    (on :open ws (lambda (&rest args) (apply #'on-ws-open conn args)))
    (on :message ws (lambda (&rest args) (apply #'on-ws-message conn args)))
    (on :error ws (lambda (&rest args) (apply #'on-ws-error conn args)))
    (on :close ws (lambda (&rest args) (apply #'on-ws-close conn args)))))

(defun make-connection (&optional old-session &rest login-options)
  "Makes a new WhatsApp Web connection object. You must call START-CONNECTION on it to actually do anything, after binding your event handlers.
If OLD-SESSION is provided, attempts to resume an old connection."
  (let* ((client-id (if old-session (session-client-id old-session) (make-client-id)))
         (conn (make-instance 'whatsapp-connection
                              :login-information login-options
                              :client-id client-id
                              :keypair (multiple-value-list (crypto:generate-key-pair :curve25519)))))
    (setf (wac-ping-timer conn) (trivial-timers:make-timer
                                 (lambda () (on-ping-timer conn))
                                 :name "ping sending timer"))
    (setf (wac-response-timer conn) (trivial-timers:make-timer
                                     (lambda () (on-ping-timeout conn))
                                     :name "ping response timer"))
    (when old-session
      (setf (wac-session conn) old-session))
    conn))

(defun close-connection (conn)
  "Closes the supplied WhatsApp Web connection object CONN."
  (check-type conn whatsapp-connection)
  (with-wac-lock (conn)
    (wsd:close-connection (wac-ws conn))
    (mitigate-wsd-brokenness conn)))

(defun start-connection (conn)
  "Starts the supplied WhatsApp Web connection object CONN. If a connection is already in progress, aborts it and reconnects."
  (check-type conn whatsapp-connection)
  (with-wac-lock (conn)
    (with-accessors ((ws wac-ws)) conn
      (when ws
        (wsd:close-connection ws))
      (setf ws (wsd:make-client *websocket-url*
                                :additional-headers *websocket-headers*))
      (bind-ws-events conn)
      (wsd:start-connection ws)))
  t)
