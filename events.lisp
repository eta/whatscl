;; Stuff that can happen on a WhatsApp connection.

(in-package :whatscl)

(defclass wac-event () ()
  (:documentation "Base class for all WhatsApp connection events."))

(defclass event-ping-timeout (wac-event) ()
  (:documentation "Event signifying that a ping timeout has occurred."))

(defclass event-qrcode-obtained (wac-event)
  ((qrcode
    :reader qrcode
    :initarg :qrcode
    :documentation "The text to go into the rendered QR code."))
  (:documentation "A QR code has been obtained from WhatsApp."))

(defclass event-raw-connection-ack (wac-event)
  ((raw
    :reader raw
    :initarg :raw
    :documentation "The raw connection ack message."))
  (:documentation "A 'connection acknowledgement' key generation message was received."))

(defclass event-connected (wac-event)
  ((jid
    :reader jid
    :initarg :jid
    :documentation "The user's JID, AS A STRING (!)."))
  (:documentation "Emitted when the user has logged in."))

(defclass event-contacts (wac-event)
  ((contacts
    :reader contacts
    :initarg :contacts
    :documentation "A list of WHATSCL::CONTACT objects."))
  (:documentation "Contains a list of WhatsApp contacts, sent by the server after login."))

(defclass event-chats (wac-event)
  ((chats
    :reader chats
    :initarg :chats
    :documentation "A list of WHATSCL::CHAT-ENTRY objects."))
  (:documentation "Contains a list of open WhatsApp chats, sent by the server after login."))

(defclass event-chat-message (wac-event)
  ((message
    :reader message
    :initarg :message
    :documentation "A WHATSCL::MESSAGE object.")
   (delivery-type
    :reader delivery-type
    :initarg :delivery-type
    :documentation "A symbol denoting what kind of delivery event this is. FIXME: explain types"))
  (:documentation "A chat message was received from WhatsApp."))

(defclass event-contact-add (wac-event)
  ((contact
    :reader contact
    :initarg :contact
    :documentation "A WHATSCL::CONTACT object."))
  (:documentation "The user added a new WhatsApp contact."))

(defclass event-picture-change (wac-event)
  ((jid
    :reader jid
    :initarg :jid
    :documentation "The JID of the user.")
   (was-removed
    :reader was-removed
    :initarg :was-removed
    :documentation "T if the profile picture was removed."))
  (:documentation "A user changed their profile picture, or removed it."))

(defclass event-presence (wac-event)
  ((of
    :reader presence-of
    :initarg :of
    :documentation "The JID of the user sending presence, or group presence is sent in (if PARTICIPANT is set)")
   (type
    :reader presence-type
    :initarg :type
    :documentation "The type of presence (usually a symbol)")
   (participant
    :reader participant
    :initarg :participant
    :documentation "The user inside the group sending presence.")
   (denied
    :reader denied
    :initarg :denied
    :documentation "Whether presence broadcasts are blocked from this user or not."))
  (:documentation "Presence information was received for a user."))

(defclass event-message-acked (wac-event)
  ((msgid
    :reader mswgid
    :initarg :msgid
    :documentation "The ID of the message being acked.")
   (ack-level
    :reader ack-level
    :initarg :ack-level
    :documentation "The level of the ack, as a symbol.")
   (from
    :reader from
    :initarg :from)
   (to
    :reader to
    :initarg :to)
   (participant
    :reader participant
    :initarg :participant)
   (part-of-many-p
    :reader part-of-many-p
    :initform nil
    :initarg :part-of-many-p)
   (ts
    :reader ts
    :initarg :ts))
  (:documentation "A message was acknowledged."))
