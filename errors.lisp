;;;; Condition types for errors returned by WhatsApp

(in-package :whatscl)

(define-condition status-code-error (error)
  ((status-code
    :initarg :status-code
    :reader scerror-status-code)
   (operation
    :initarg :operation
    :reader scerror-operation))
  (:report (lambda (condit stream)
             (with-slots (status-code operation) condit
               (format stream "~A returned wrong status code: ~A" operation status-code))))
  (:documentation "An error (with failure code STATUS-CODE) originating from some asynchronous WhatsApp operation, the nature of which is described by the keyword OPERATION.
Unless the error is also of type LOGIN-ERROR, these conditions are often non-fatal and can be ignored."))

(define-condition login-error (status-code-error) ()
  (:report (lambda (condit stream)
             (with-slots (status-code operation) condit
               (format stream "Login to WhatsApp failed during stage ~A with code ~A" operation status-code))))
  (:documentation "Subclass of STATUS-CODE-ERROR that indicates a failure to log into WhatsApp."))

(defun cb-check-status (conn details &optional (reason :generic-operation) (condition-type 'status-code-error))
  "Verifies that the included DETAILS object contains a :status key with value 200. If not, signals a condition of type CONDITION-TYPE, using the provide REASON as the OPERATION slot."
  (declare (ignore conn))
  (unless (eql (aval :status details) 200)
    (error (find-class condition-type)
           :operation reason
           :status-code (aval :status details))))

(defmacro function-check-status (&optional reason (condition-type '(quote status-code-error)))
  "Returns a lambda taking (conn details) that calls CB-CHECK-STATUS with (conn details reason)."
  (let ((conn-sym (gensym))
        (details-sym (gensym)))
    `(lambda (,conn-sym ,details-sym)
       (cb-check-status ,conn-sym ,details-sym ,reason ,condition-type))))
