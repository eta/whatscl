;;;; Cryptographic functions used in the protocol

(in-package :whatscl)

(defun hkdf (key length &optional app-info)
  "Derives a key of length LENGTH from KEY, using HKDF. An APP-INFO additional information vector may also be provided."
  (let ((kdf (crypto:make-kdf :hmac-kdf
                              :digest :sha256
                              :additional-data app-info)))
    (crypto:derive-key kdf key
                       (make-array 32
                                   :element-type '(unsigned-byte 8)
                                   :initial-element 0)
                       -1 ; iteration count /should/ be ignored, right?
                       length)))

(defun sha256 (data)
  "Computes the SHA256 hash of DATA."
  (ironclad:digest-sequence :sha256 data))

(defun hmac-sha256 (key data)
  "Computes the HMAC-SHA256 message authentication code for DATA, using the secret KEY."
  (let ((mac (crypto:make-mac :hmac key :sha256)))
    (crypto:update-mac mac data)
    (crypto:produce-mac mac)))

(defmacro vectors-concatenate (&rest args)
  "Concatenate ARGS (all of type '(VECTOR (UNSIGNED-BYTE 8))) into a vector of the same type."
  `(concatenate '(vector (unsigned-byte 8)) ,@args))

(defun aes-encrypt (key iv plaintext)
  "Encrypts PLAINTEXT with KEY, using the AES-CBC cipher and the provided IV."
  (let* ((cipher (crypto:make-cipher :aes
                                     :key key
                                     :mode :cbc
                                     :padding :pkcs7
                                     :initialization-vector iv))
         (out (make-array (+ (length plaintext) 32)
                          :initial-element 0
                          :element-type '(unsigned-byte 8))))
    (multiple-value-bind (consumed produced)
        (crypto:encrypt cipher plaintext out
                        :handle-final-block t)
      (declare (ignore consumed))
      (subseq out 0 produced))))

(defun aes-decrypt (key ciphertext)
  "Decrypts CIPHERTEXT with KEY, using the AES-CBC cipher."
  (let* ((block-length (crypto:block-length :aes))
         (iv (subseq ciphertext 0 block-length))
         (ciphertext (subseq ciphertext block-length))
         (cipher (crypto:make-cipher :aes
                                     :key key
                                     :mode :cbc
                                     :padding :pkcs7
                                     :initialization-vector iv))
         (out (make-array (length ciphertext)
                          :initial-element 0
                          :element-type '(unsigned-byte 8))))
    (multiple-value-bind (consumed produced)
        (crypto:decrypt cipher ciphertext out
                        :handle-final-block t)
      (declare (ignore consumed))
      (subseq out 0 produced))))


(defun keys-from-secret (secret keypair)
  "Given a base64-encoded SECRET from the server, extract the encKey and macKey from it using KEYPAIR.
Returns the encKey and macKey as two values."
  (let* ((secret (qbase64:decode-string secret))
         (pubkey (crypto:make-public-key :curve25519
                                         :y (subseq secret 0 32)))
         (shared-secret (crypto:diffie-hellman (car keypair) pubkey))
         (expanded-secret (hkdf shared-secret 80))
         (hmac-expected (subseq secret 32 64))
         (hmac-derived (hmac-sha256
                        (subseq expanded-secret 32 64)
                        (vectors-concatenate (subseq secret 0 32) (subseq secret 64))))
         (keys-encrypted (vectors-concatenate (subseq expanded-secret 64) (subseq secret 64))))
    (assert (equalp hmac-derived hmac-expected) () "HMAC validation failed:~%expected ~A~%got ~A" hmac-expected hmac-derived)
    (let ((keys-decrypted (aes-decrypt (subseq expanded-secret 0 32) keys-encrypted)))
      (values (subseq keys-decrypted 0 32) (subseq keys-decrypted 32)))))

(defun validate-and-decrypt-binary-ws-message (session message)
  "Using the provided SESSION information, validate and decrypt the binary WebSocket MESSAGE, throwing an error if validation fails."
  (let ((hmac-expected (subseq message 0 32))
        (hmac-derived (hmac-sha256 (session-mac-key session) (subseq message 32)))
        (decrypted (aes-decrypt (session-enc-key session) (subseq message 32))))
    (assert (equalp hmac-derived hmac-expected) () "HMAC validation failed:~%expected ~A~%got ~A" hmac-expected hmac-derived)
    decrypted))

(defun sign-and-encrypt-binary-ws-message (session message)
  "Using the provided SESSION information, sign and encrypt the binary WebSocket MESSAGE."
  (declare (type (simple-array (unsigned-byte 8)) message))
  (let* ((block-length (crypto:block-length :aes))
         (iv (crypto:random-data block-length))
         (encrypted-message (aes-encrypt (session-enc-key session) iv message))
         (iv-and-message (vectors-concatenate iv encrypted-message))
         (signature (hmac-sha256 (session-mac-key session) iv-and-message)))
    (vectors-concatenate signature iv-and-message)))

(defun get-media-app-info (type)
  "Gets the HKDF application info for the media type symbol TYPE."
  (declare (type (member :image :video :audio :document) type))
  (babel:string-to-octets
   (ecase type
     (:image "WhatsApp Image Keys")
     (:video "WhatsApp Video Keys")
     (:audio "WhatsApp Audio Keys")
     (:document "WhatsApp Document Keys"))))

(defun decrypt-media-data (media-key data type)
  "Decrypt DATA, a piece of encrypted media data, using the provided MEDIA-KEY.
TYPE is a symbol (one of :IMAGE, :VIDEO, :AUDIO or :DOCUMENT) specifying what type of data it is."
  (declare (type (simple-array (unsigned-byte 8)) media-key data)
           (type (member :image :video :audio :document) type))
  (let* ((appinfo (get-media-app-info type))
         (media-key-expanded (hkdf media-key 112 appinfo))
         (iv (subseq media-key-expanded 0 16))
         (cipher-key (subseq media-key-expanded 16 48))
         (mac-key (subseq media-key-expanded 48 80))
         (mac-split (- (length data) 10))
         (file-data (subseq data 0 mac-split))
         (hmac-expected (subseq data mac-split))
         (iv-and-file (vectors-concatenate iv file-data))
         (hmac-derived (hmac-sha256 mac-key iv-and-file))
         (decrypted (aes-decrypt cipher-key iv-and-file)))
    (assert (equalp hmac-expected (subseq hmac-derived 0 10)) ()
            "HMAC validation failed~%expected ~A~%got ~A" hmac-derived hmac-expected)
    decrypted))

(defun generate-media-keys (type)
  "Generate a 32-byte random media key, and then expand it using the provided media TYPE (one of :IMAGE, :VIDEO, :AUDIO, or :DOCUMENT).
Returns (RANDOM-KEY IV CIPHER-KEY MAC-KEY), with the latter 3 values taken as subsequences of the expanded key."
  (check-type type (member :image :video :audio :document))
  (let* ((random-key (crypto:random-data 32))
         (expanded (hkdf random-key 112 (get-media-app-info type))))
    (values random-key
            (subseq expanded 0 16)
            (subseq expanded 16 48)
            (subseq expanded 48 80))))

(defun encrypt-media-data (data type)
  "Encrypt the provided DATA (a piece of encrypted media data), a piece of media of the given TYPE (one of :IMAGE, :VIDEO, :AUDIO, or :DOCUMENT).
Returns (ENCRYPTED-BLOB RANDOM-KEY FILE-SHA256 FILE-ENC-SHA256), where ENCRYPTED-BLOB should be uploaded to WhatsApp (along with a base64-encoded FILE-ENC-SHA256), and the other fields should be used to populate an instance of class FILE-INFO."
  (check-type data (simple-array (unsigned-byte 8)))
  (check-type type (member :image :video :audio :document))
  (multiple-value-bind (random-key iv cipher-key mac-key)
      (generate-media-keys type)
    (let* ((enc (aes-encrypt cipher-key iv data))
           (mac (subseq (hmac-sha256 mac-key
                                     (vectors-concatenate iv enc))
                        0 10))
           (file-sha-256 (sha256 data))
           (enc-mac (vectors-concatenate enc mac))
           (file-enc-sha-256 (sha256 enc-mac)))
      (values enc-mac random-key file-sha-256 file-enc-sha-256))))
