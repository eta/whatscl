{ depot ? import <depot> {}, lispPkgs ? {} }:

with depot.nix;
with depot.third_party.lisp;
with lispPkgs;

buildLisp.library {
  name = "whatscl";
  deps = [ (buildLisp.bundled "asdf") event-emitter alexandria split-sequence  ironclad qbase64 babel cl-json nibbles flexi-streams protobuf local-time wsd-client trivial-timers trivial-backtrace ];
  srcs = map (f: ./. + ("/" + f)) [
    "packages.lisp"
    "utf-16.lisp"
    "utils.lisp"
    "crypto.lisp"
    "connection.lisp"
    "binproto.lisp"
    "message_wire.lisp"
    "tables.lisp"
    "message.lisp"
    "api.lisp"
    "errors.lisp"
    "whatscl.lisp"
  ];
}
