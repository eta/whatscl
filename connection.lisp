;;;; Defines a connection class, representing a WhatsApp Web connection.

(in-package :whatscl)

(defclass persistent-session ()
  ((client-token
    :initarg :client-token
    :reader session-client-token)
   (server-token
    :initarg :server-token
    :reader session-server-token)
   (client-id
    :initarg :client-id
    :reader session-client-id)
   (enc-key
    :initarg :enc-key
    :reader session-enc-key)
   (mac-key
    :initarg :mac-key
    :reader session-mac-key))
  (:documentation "Necessary information required to re-establish an old WhatsApp Web connection."))

(defun serialize-persistent-session (sess)
  "Serialize the provided PERSISTENT-SESSION object to a string which can later be passed to DESERIALIZE-PERSISTENT-SESSION."
  (with-output-to-string (s)
    (cl-json:encode-json sess s)))

(defun deserialize-persistent-session (ser)
  "Deserialize the serialized PERSISTENT-SESSION stored in SER and return it as a proper object."
  (let ((ser (cl-json:decode-json (make-string-input-stream ser))))
    (make-instance 'persistent-session
                   :client-token (aval :client-token ser)
                   :server-token (aval :server-token ser)
                   :client-id (aval :client-id ser)
                   :enc-key (coerce (aval :enc-key ser) '(vector (unsigned-byte 8)))
                   :mac-key (coerce (aval :mac-key ser) '(vector (unsigned-byte 8))))))

(defclass whatsapp-connection (event-emitter)
  ((lock
    :accessor wac-lock
    :initform (bt:make-recursive-lock "WhatsApp connection lock"))
   (ws
    :initform nil
    :accessor wac-ws)
   (session
    :initarg :session
    :initform nil
    :accessor wac-session)
   (jid
    :initform nil
    :accessor wac-jid)
   (outgoing
    :initform (make-hash-table :test #'equal)
    :accessor wac-outgoing)
   (login-information
    :initarg :login-information
    :initform nil
    :reader wac-login-information)
   (client-id
    :initarg :client-id
    :reader wac-client-id)
   (keypair
    :initarg :keypair
    :initform nil
    :accessor wac-keypair)
   (is-connected
    :initform nil
    :accessor wac-is-connected)
   (ping-timer
    :initform nil
    :accessor wac-ping-timer)
   (response-timer
    :initform nil
    :accessor wac-response-timer)
   (outgoing-queue
    :initform nil
    :accessor wac-outgoing-queue)
   (epoch
    :initform 0
    :accessor wac-epoch))
  (:documentation "Represents a WhatsApp Web connection."))

(defun get-wac-session (wac)
  "Returns the PERSISTENT-SESSION object stored in WAC, a `whatsapp-connection' object."
  (wac-session wac))

(defun get-wac-jid (wac)
  "Returns the JID stored in WAC, a `whatsapp-connnection' object."
  (wac-jid wac))

(defmacro with-wac-lock ((wac) &body body)
  "Evaluates BODY with the lock in the `whatsapp-connection' object WAC held.
Functions like BT:WITH-RECURSIVE-LOCK with respect to non-local transfers of control (i.e. the lock is always released, etc.)"
  `(bt:with-recursive-lock-held ((wac-lock ,wac))
     ,@body))
