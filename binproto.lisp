;;;; Binary message protocol decoding

(in-package :whatscl)

(defparameter *tokens*
  #("200" "400" "404" "500" "501" "502" "action" "add"
    "after" "archive" "author" "available" "battery" "before" "body"
    "broadcast" "chat" "clear" "code" "composing" "contacts" "count"
    "create" "debug" "delete" "demote" "duplicate" "encoding" "error"
    "false" "filehash" "from" "g.us" "group" "groups_v2" "height" "id"
    "image" "in" "index" "invis" "item" "jid" "kind" "last" "leave"
    "live" "log" "media" "message" "mimetype" "missing" "modify" "name"
    "notification" "notify" "out" "owner" "participant" "paused"
    "picture" "played" "presence" "preview" "promote" "query" "raw"
    "read" "receipt" "received" "recipient" "recording" "relay"
    "remove" "response" "resume" "retry" "c.us" "seconds"
    "set" "size" "status" "subject" "subscribe" "t" "text" "to" "true"
    "type" "unarchive" "unavailable" "url" "user" "value" "web" "width"
    "mute" "read_only" "admin" "creator" "short" "update" "powersave"
    "checksum" "epoch" "block" "previous" "409" "replaced" "reason"
    "spam" "modify_tag" "message_info" "delivery" "emoji" "title"
    "description" "canonical-url" "matched-text" "star" "unstar"
    "media_key" "filename" "identity" "unread" "page" "page_count"
    "search" "media_message" "security" "call_log" "profile" "ciphertext"
    "invite" "gif" "vcard" "frequent" "privacy" "blacklist" "whitelist"
    "verify" "location" "document" "elapsed" "revoke_invite" "expiration"
    "unsubscribe" "disable" "vname" "old_jid" "new_jid" "announcement"
    "locked" "prop" "label" "color" "call" "offer" "call-id"
    "quick_reply" "sticker" "pay_t" "accept" "reject" "sticker_pack" "invalid"
    "canceled" "missed" "connected" "result" "audio" "video" "recent")
  "Tokens (which are like interned strings) for use in the binary protocol.")

(defmacro define-binary-constants (&body constants)
  `(progn
     ,@(loop for (k v) in constants
             collect `(define-symbol-macro ,k ,v))))

(define-binary-constants
  (+list-empty+ 0)
  (+stream-8+ 2)
  (+dictionary-0+ 236)
  (+dictionary-1+ 237)
  (+dictionary-2+ 238)
  (+dictionary-3+ 239)
  (+list-8+ 248)
  (+list-16+ 249)
  (+jid-pair+ 250)
  (+hex-8+ 251)
  (+binary-8+ 252)
  (+binary-20+ 253)
  (+binary-32+ 254)
  (+nibble-8+ 255))

(defclass jid ()
  ((localpart
    :initarg :localpart
    :accessor jid-localpart)
   (hostname
    :initarg :hostname
    :accessor jid-hostname))
  (:documentation "A Jabber ID."))

(defmethod print-object ((obj jid) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (localpart hostname) obj
      (format stream "~A@~A" localpart hostname))))

(defun parse-jid (string)
  "Parse STRING into a JID object."
  (let ((at-pos (position #\@ string)))
    (unless at-pos
      (error "~A contains no @ character" string))
    (multiple-value-bind (localpart hostname)
        (split-at string at-pos)
      (unless (> (length hostname) 0)
        (error "~A contains no hostname" string))
      (make-jid localpart hostname))))

(defun make-jid (localpart hostname)
  "Make a JID for LOCALPART@HOSTNAME."
  (make-instance 'jid
                 :localpart localpart
                 :hostname hostname))

(defun jid-to-string (jid)
  "Convert JID to a string."
  (with-slots (localpart hostname) jid
    (let ((hostname (if (equal hostname "s.whatsapp.net") "c.us" hostname)))
      (concatenate 'string localpart "@" hostname))))

(defun jid-to-message-target (jid)
  "Convert JID to a string, for use in making a message target."
  (with-slots (localpart hostname) jid
    ;; Here's a funny story: having an invalid JID hostname here sends
    ;; WhatsApp into a crash loop (!), requiring a reinstall.
    ;; 4 reinstalls later and I figured out the bug...
    (let ((hostname (if (equal hostname "g.us") "g.us" "s.whatsapp.net")))
      (concatenate 'string localpart "@" hostname))))

(defun nibble-to-denary-char (nib)
  "Converts NIB, a nibble, into a character representing that value in denary (with some punctuation exceptions as well)."
  (declare (type (integer 0 16) nib))
  (cond
    ((eql nib 10) #\-)
    ((eql nib 11) #\.)
    ((eql nib 15) #\Nul)
    (t (code-char (+ nib (char-code #\0))))))

(defun denary-char-to-nibble (ch)
  "Convers CH, a denary character, to a nibble."
  (declare (type character ch))
  (ecase ch
    (#\0 0)
    (#\1 1)
    (#\2 2)
    (#\3 3)
    (#\4 4)
    (#\5 5)
    (#\6 6)
    (#\7 7)
    (#\8 8)
    (#\9 9)
    (#\- 10)
    (#\. 11)
    (#\Nul 15)))

(defun nibble-to-hex-char (nib)
  "Converts NIB, a nibble, into a character representing that value in hex."
  (declare (type (integer 0 16) nib))
  (code-char
   (cond
     ((< nib 10) (+ nib (char-code #\0)))
     (t (+ (- nib 10) (char-code #\A))))))

(defun read-nibbles (stream num-bytes)
  "Reads NUM-BYTES bytes from STREAM, and returns NUM-BYTES * 2 nibbles."
  (loop
    repeat num-bytes
    append (let ((b (read-byte stream)))
             (list (ash (logand b #xF0) -4) (logand b #x0F)))))

(defun write-nibbles (stream nibbles)
  "Writes NIBBLES (a vector) to STREAM as bytes."
  (declare (type vector nibbles))
  (multiple-value-bind (num-bytes one-left)
      (floor (length nibbles) 2)
    (loop
      for i from 0 below num-bytes
      do (let* ((idx1 (* i 2))
                (idx2 (1+ idx1))
                (nib1 (elt nibbles idx1))
                (nib2 (elt nibbles idx2)))
           (write-byte
            (logior (ash nib1 4) nib2)
            stream)))
    (when (> one-left 0)
      (write-byte
       (+ 15
          (ash (elt nibbles (1- (length nibbles))) 4))
       stream))))

(defun read-packed-8 (stream hex-p)
  "Reads a string in 'packed-8' format from STREAM, reading the values in hexadecimal instead of denary if HEX-P is T."
  (let* ((start (read-byte stream))
         (num-iterations (mod start 128))
         (one-less-p (>= start 128))
         (conversion-func (if hex-p #'nibble-to-hex-char #'nibble-to-denary-char))
         (ret (loop
                for nib in (read-nibbles stream num-iterations)
                collect (funcall conversion-func nib))))
    (subseq (coerce ret 'string) 0
            (if one-less-p (1- (length ret)) (length ret)))))

(defun write-packed-8 (stream str)
  "Write a string in denary 'packed-8' format to STREAM."
  (let* ((num-bytes (floor (1+ (length str)) 2))
         (actual-len (logior
                      ;; If the length of the string is odd,
                      ;; set the MSB of the `len' value
                      ;; (this is `one-less-p' above).
                      (if (eql (mod (length str) 2) 1)
                          128
                          0)
                      num-bytes))
         (nibbles (map 'vector #'denary-char-to-nibble str)))
    (write-byte actual-len stream)
    (write-nibbles stream nibbles)))

(defun read-int20 (stream)
  "Reads a 20-bit big-endian integer from STREAM."
  (flet ((next-byte () (read-byte stream)))
    (logior (ash (logand (next-byte) #x0f) 16) (ash (next-byte) 8) (next-byte))))

(defun write-int20 (stream value)
  "Writes a 20-bit big-endian integer VALUE to STREAM."
  (write-byte (logand (ash value -16) #xFF) stream)
  (write-byte (logand (ash value -8) #xFF) stream)
  (write-byte (logand value #xFF) stream))

(defun read-octets (stream length)
  "Reads LENGTH bytes from STREAM, and returns them."
  (let ((ret (make-array length :element-type '(unsigned-byte 8))))
    (assert (eql (read-sequence ret stream) length) () "EOF while reading ~A bytes." length)
    ret))

(defun read-string (stream length)
  "Reads LENGTH bytes from STREAM, and converts the result into a string."
  (babel:octets-to-string (read-octets stream length)))

(defun write-octets (stream octets)
  "Writes OCTETS to STREAM."
  (declare (type (vector (unsigned-byte 8)) octets))
  (let ((len (length octets)))
    (cond
      ((and (>= len 0) (<= len 255))
       (progn
         (write-byte +binary-8+ stream)
         (write-byte len stream)))
      ((and (>= len 256) (<= len 1048575)) ; (1- (expt 2 20))
       (progn
         (write-byte +binary-20+ stream)
         (write-int20 stream len)))
      (t
       (write-byte +binary-32+ stream)
       (nibbles:write-ub32/be len stream)))
    (write-sequence octets stream)))

(defun write-binary-string (stream string)
  "Writes STRING to STREAM."
  (write-octets stream (babel:string-to-octets string)))

(defun read-list-size (stream tag)
  "Reads a list size, of type designated by the value of TAG, from STREAM."
  (cond
    ((eql tag +list-empty+) 0)
    ((eql tag +list-8+) (read-byte stream))
    ((eql tag +list-16+) (nibbles:read-ub16/be stream))
    (t (error "Invalid list size tag: ~A" tag))))

(defun write-list-size (stream size)
  "Writes a list size SIZE to STREAM."
  (cond
    ((eql size 0) (write-byte +list-empty+ stream))
    ((and (>= size 1) (<= size 256))
     (progn
       (write-byte +list-8+ stream)
       (write-byte size stream)))
    (t
     (progn
       (write-byte +list-16+ stream)
       (nibbles:write-ub16/be size stream)))))

(defun read-list (stream n-elements)
  "Read a list of N-ELEMENTS nodes from STREAM."
  (loop
    repeat n-elements
    collect (read-node stream)))

(defun write-list (stream nodes)
  "Write NODES, a list of nodes, to STREAM."
  (write-list-size stream (length nodes))
  (loop
    for node in nodes
    do (write-node node stream)))

(defun read-proto-value (stream tag &optional binary)
  "Reads a protocol value, of type designated by the value of TAG, from STREAM.
If BINARY is true, interprets +BINARY-N+ tags as needing to read octets, not characters."
  (let ((binary-func (if binary #'read-octets #'read-string)))
    (cond
      ((and (>= tag 3) (<= tag 235)) (aref *tokens* (- tag 3)))
      ;; The following line is supposed to deal with 'double tokens' --
      ;; but nobody appears to have a list of these, so we just read a byte
      ;; and return an empty list.
      ((and (>= tag +dictionary-0+) (<= tag +dictionary-3+))
       (warn "Unknown double token: ~A" (read-byte stream)) '())
      ((eql tag +list-empty+) nil)
      ((or (eql tag +list-8+) (eql tag +list-16+)) (read-list stream (read-list-size stream tag)))
      ((eql tag +binary-8+) (funcall binary-func stream (read-byte stream)))
      ((eql tag +binary-20+) (funcall binary-func stream (read-int20 stream)))
      ((eql tag +binary-32+) (funcall binary-func stream (nibbles:read-ub32/be stream)))
      ((eql tag +jid-pair+)
       (flet ((read-value () (read-proto-value stream (read-byte stream))))
         (make-jid (read-value) (read-value))))
      ((eql tag +nibble-8+) (read-packed-8 stream nil))
      ((eql tag +hex-8+) (read-packed-8 stream t))
      (t (error "Invalid tag: ~A" tag)))))

(defun write-proto-value (stream value)
  "Write the protocol value VALUE to STREAM."
  (etypecase value
    (null
     (write-list stream nil))
    (string
     (let ((token (position value *tokens*
                            :test #'string-equal)))
       (if token
           (write-byte (+ token 3) stream)
           (write-binary-string stream value))))
    (symbol
     (write-proto-value stream (string-downcase (symbol-name value))))
    (jid
     (write-byte +jid-pair+ stream)
     (write-byte +nibble-8+ stream)
     (write-packed-8 stream (jid-localpart value))
     (let* ((jh (jid-hostname value))
            ;; Other hostnames cause WhatsApp to freak out, so we restrict
            ;; it to these two. (g.us always means g.us; s.whatsapp.net
            ;; and c.us mean c.us)
            (hostname-to-write (if (equal jh "g.us") "g.us" "c.us")))
       (write-proto-value stream hostname-to-write)))
    ((vector (unsigned-byte 8))
     (write-octets stream value))
    (list
     (write-list stream value))))

(defun tentative-keywordize (value)
  "If VALUE is a string, intern it in the KEYWORD package and return the interned symbol. Otherwise, return it as is, and log a warning."
  (if (stringp value)
      (intern (string-upcase value) :keyword)
      (progn
        (warn "Couldn't keywordize: ~A" value)
        value)))

(defun read-attributes (stream num-attrs)
  "Reads NUM-ATTRS attribute key-value pairs from STREAM and returns them."
  (flet ((read-attr () (read-proto-value stream (read-byte stream))))
    (loop
      repeat num-attrs
      collect (cons (tentative-keywordize (read-attr))
                    (read-attr)))))

(defun read-node (stream)
  "Reads a node from STREAM and returns it."
  (let* ((list-size (read-list-size stream (read-byte stream)))
         (descr (read-proto-value stream (read-byte stream)))
         (attrs (read-attributes stream (ash (+ (- list-size 2) (mod list-size 2)) -1)))
         (content (unless (oddp list-size)
                    (read-proto-value stream (read-byte stream) t))))
    (list (tentative-keywordize descr) attrs content)))

(defun write-node (node stream)
  "Probably buggy function to write the node NODE to STREAM."
  (destructuring-bind (desc attrs &optional content) node
    (let ((list-size (+
                      (if (null content) 1 2)
                      (* (length attrs) 2))))
      (write-list-size stream list-size)
      (write-proto-value stream desc)
      (loop
        for (attr . value) in attrs
        do (progn
             (write-proto-value stream attr)
             (write-proto-value stream value)))
      (unless (null content)
        (write-proto-value stream content)))))

(defun deserialize-binary-message (buf)
  "Deserializes the binary WhatsApp Web message in BUF and returns it as a Lisp object."
  (read-node (flexi-streams:make-in-memory-input-stream buf)))

(defun serialize-binary-message (node)
  "Serializes the WhatsApp Web node in NODE to a simple array."
  (let ((stream (flexi-streams:make-in-memory-output-stream)))
    (write-node node stream)
    (coerce (flexi-streams:get-output-stream-sequence stream)
            '(simple-array (unsigned-byte 8) (*)))))
