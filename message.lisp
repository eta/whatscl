;; Parsing messages into something useful, maybe.

(in-package :whatscl)

(defclass message-key ()
  ((jid
    :initarg :jid
    :accessor key-jid
    :documentation "Some JID. Its meaning is given in the class documentation."))
  (:documentation "Represents a message header, containing a JID. This class is never instantiated directly."))

(defclass message-key-sending (message-key) ()
  (:documentation "Represents a message header where we sent the message, and the JID is the recipient."))

(defmethod print-object ((obj message-key-sending) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (jid) obj
      (format stream "SENT to ~A" jid))))

(defclass message-key-receiving (message-key) ()
  (:documentation "Represents a message header where we received the message, and the JID is the sender."))

(defmethod print-object ((obj message-key-receiving) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (jid) obj
      (format stream "RECEIVED to ~A" jid))))

(defclass message-key-group-receiving (message-key)
  ((participant
    :initarg :participant
    :accessor key-participant
    :documentation "The JID of the message sender."))
  (:documentation "Represents a message header where we received the message from a group. The JID is the group's JID, and the participant field is the JID of the sender."))

(defmethod print-object ((obj message-key-group-receiving) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (participant jid) obj
      (format stream "RECEIVED from ~A in ~A" participant jid))))

(defclass message-contents () ()
  (:documentation "Parent class for all message content objects."))

(defclass message-contents-stub (message-contents)
  ((stub-type
    :initarg :stub-type
    :accessor contents-stub-type
    :documentation "A symbol describing what type of stub it is.")
   (stub-parameters
    :initarg :stub-parameters
    :accessor contents-stub-parameters
    :documentation "A list of parameters for the stub message."))
  (:documentation "A stub message."))

(defmethod print-object ((obj message-contents-stub) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (stub-type stub-parameters) obj
      (format stream "~A ~A" stub-type stub-parameters))))

(defclass message-contents-empty (message-contents)
  ()
  (:documentation "Empty contents (for when a message object isn't provided)."))

(defmethod print-object ((obj message-contents-empty) stream)
  (print-unreadable-object (obj stream :type t)
    (format stream "")))

(defclass message-contents-text (message-contents)
  ((text
    :initarg :text
    :accessor contents-text
    :documentation "The text of the message."))
  (:documentation "Simple text-based contents."))

(defmethod print-object ((obj message-contents-text) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (text) obj
      (format stream "\"~A\"" text))))

(defclass message-contents-unknown (message-contents)
  ((raw
    :initarg :raw
    :accessor contents-raw
    :documentation "The raw protobuf message object."))
  (:documentation "Some message contents we don't know how to parse yet."))

(defmethod print-object ((obj message-contents-unknown) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (raw) obj
      (format stream "~A" raw))))

(defclass file-info ()
  ((url
    :initarg :url
    :accessor file-info-url)
   (mime-type
    :initarg :mime-type
    :accessor file-info-mime-type)
   (sha256
    :initarg :sha256
    :accessor file-info-sha256)
   (enc-sha256
    :initarg :enc-sha256
    :accessor file-info-enc-sha256)
   (length-bytes
    :initarg :length-bytes
    :accessor file-info-length-bytes)
   (media-key
    :initarg :media-key
    :accessor file-info-media-key))
  (:documentation "Information about an encrypted file."))

(defun extract-file-info (msg)
  "Extract a FILE-INFO object from MSG, some type of protobuf message object."
  (make-instance 'file-info
                 :url (pb:string-value (wpb:url msg))
                 :mime-type (pb:string-value (wpb:mimetype msg))
                 :sha256 (wpb:file-sha256 msg)
                 :enc-sha256 (wpb:file-enc-sha256 msg)
                 :length-bytes (wpb:file-length msg)
                 :media-key (wpb:media-key msg)))

(defclass message-contents-file (message-contents)
  ((file-info
    :initarg :file-info
    :accessor contents-file-info
    :documentation "The FILE-INFO object for the enclosed file."))
  (:documentation "An encrypted file sent by another user. Various subclasses exist for images, audio, etc."))

(defmethod print-object ((obj message-contents-file) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (file-info) obj
      (format stream "~A" (file-info-mime-type file-info)))))

(defclass message-contents-audio (message-contents-file)
  ((duration-secs
    :initarg :duration-secs
    :initform nil
    :accessor contents-duration-secs
    :documentation "If the file is audio or video, its duration in seconds.")))

(defclass message-contents-video (message-contents-file)
  ((duration-secs
    :initarg :duration-secs
    :initform nil
    :accessor contents-duration-secs
    :documentation "If the file is audio or video, its duration in seconds.")
   (caption
    :initarg :caption
    :initform nil
    :accessor contents-caption
    :documentation "If the file is an image or video, a possible text caption.")))

(defclass message-contents-document (message-contents-file)
  ((filename
    :initarg :filename
    :accessor contents-filename
    :documentation "The provided document filename.")))

(defclass message-contents-image (message-contents-file)
  ((height-px
    :initarg :height-px
    :accessor contents-height-px
    :documentation "The image height, in pixels.")
   (width-px
    :initarg :width-px
    :accessor contents-width-px
    :documentation "The image width, in pixels.")
   (jpeg-thumbnail
    :initarg :jpeg-thumbnail
    :accessor contents-jpeg-thumbnail
    :documentation "A JPEG image thumbnail.")
   (caption
    :initarg :caption
    :initform nil
    :accessor contents-caption
    :documentation "If the file is an image or video, a possible text caption.")))

(defmethod contents-caption ((m message-contents-file))
  (declare (ignore m))) ; Let's be helpful

(defmacro extract-file-message (msg-obj class-name &rest initargs)
  "Extract a subclass of MESSAGE-CONTENTS-FILE named CLASS-NAME from MSG-OBJ, passing the addiitonal INITARGS to MAKE-INSTANCE.
INITARGS are given in the form ((:INITARG . FUNC)), where FUNC is replaced with the value of (FUNCALL FUNC MSG-OBJ) upon expansion."
  (let* ((msg (gensym))
         (initargs (loop
                     for (ia . func) in initargs
                     append `(,ia (funcall ,func ,msg)))))
    `(let ((,msg ,msg-obj))
       (make-instance ,class-name
                      :file-info (extract-file-info ,msg)
                      ,@initargs))))

(defclass message-contents-contact (message-contents)
  ((display-name
    :initarg :display-name
    :accessor contents-display-name
    :documentation "A display name for the contact.")
   (vcard
    :initarg :vcard
    :accessor contents-vcard
    :documentation "The contact's information, in vCard format.")))

(defclass message-contents-redaction (message-contents)
  ((redacted-id
    :initarg :redacted-id
    :accessor contents-redacted-id
    :documentation "The message ID of the message that was redacted.")))

(defclass message ()
  ((key
    :initarg :key
    :accessor message-key)
   (id
    :initarg :id
    :accessor message-id)
   (ts
    :initarg :ts
    :accessor message-ts)
   (status
    :initarg :status
    :accessor message-status)
   (contents
    :initarg :contents
    :accessor message-contents)
   (raw
    :initarg :raw
    :accessor message-raw)
   (quoted-participant
    :initarg :quoted-participant
    :accessor message-quoted-participant)
   (quoted-contents
    :initarg :quoted-contents
    :accessor message-quoted-contents)))

(defmethod print-object ((obj message) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (key id ts status contents) obj
      (format stream "~A~%~A~%ts: ~A~%status: ~A~%contents: ~A" id key ts status contents))))

(defun get-contents-media-type (contents)
  "Returns a symbol specifying the 'media type' of CONTENTS (to be passed to DECRYPT-MEDIA-DATA), or NIL if CONTENTS does not contain media."
  (typecase contents
    (message-contents-image :image)
    (message-contents-video :video)
    (message-contents-audio :audio)
    (message-contents-document :document)
    (t nil)))

(defun parse-non-stub-message-contents (msg)
  "Extract a MESSAGE-CONTENTS object from MSG, a WPB:MESSAGE object."
  (symbol-macrolet
      ((proto-msg (wpb:protocol-message msg))
       (caption-func (lambda (v) (pb:string-value (wpb:caption v)))))
    (cond
      ((wpb:has-conversation msg)
       (make-instance 'message-contents-text
                      :text (pb:string-value (wpb:conversation msg))))
      ((wpb:has-extended-text-message msg)
       (make-instance 'message-contents-text
                      :text (pb:string-value (wpb:text (wpb:extended-text-message msg)))))
      ((wpb:has-image-message msg)
       (extract-file-message (wpb:image-message msg) 'message-contents-image
                             (:height-px . #'wpb:height)
                             (:width-px . #'wpb:width)
                             (:caption . caption-func)
                             (:jpeg-thumbnail . #'wpb:jpeg-thumbnail)))
      ((wpb:has-video-message msg)
       (extract-file-message (wpb:video-message msg) 'message-contents-video
                             (:caption . caption-func)
                             (:duration-secs . #'wpb:seconds)))
      ((wpb:has-audio-message msg)
       (extract-file-message (wpb:audio-message msg) 'message-contents-audio
                             (:duration-secs . #'wpb:seconds)))
      ((wpb:has-document-message msg)
       (extract-file-message (wpb:document-message msg) 'message-contents-document
                             (:filename . (lambda (v)
                                            (pb:string-value (wpb:file-name v))))))
      ((wpb:has-contact-message msg)
       (let ((cm (wpb:contact-message msg)))
         (make-instance 'message-contents-contact
                        :vcard (wpb:vcard cm)
                        :display-name (wpb:display-name cm))))
      ((and (wpb:has-protocol-message msg) (wpb:has-key proto-msg) (wpb:has-type proto-msg))
       (make-instance 'message-contents-redaction
                      :redacted-id (pb:string-value (wpb:id (wpb:key proto-msg)))))
      (t (make-instance 'message-contents-unknown
                        :raw msg)))))

(defun parse-message-contents (wmi)
  "Extract a MESSAGE-CONTENTS object from WMI, a WPB:WEB-MESSAGE-INFO object."
  (cond
    ((wpb:has-message-stub-type wmi)
     (make-instance 'message-contents-stub
                    :stub-type (symbolize-message-stub-type (wpb:message-stub-type wmi))
                    :stub-parameters (mapcar #'pb:string-value (coerce (wpb:message-stub-parameters wmi) 'list))))
    ((not (wpb:has-message wmi))
     (make-instance 'message-contents-empty))
    (t (parse-non-stub-message-contents (wpb:message wmi)))))

(defmacro extract-context-info-inner (msg &rest types)
  (let* ((msg-sym (gensym))
         (clauses (loop
                    for type in types
                    append `(((,(intern
                                 (string-upcase
                                  (concatenate 'string
                                               "HAS-" type))
                                 'wpb)
                               ,msg-sym)
                              (,(intern
                                 (string-upcase type)
                                 'wpb)
                               ,msg-sym))))))
    `(let ((,msg-sym ,msg))
       (cond
         ,@clauses
         (t nil)))))

(defun extract-context-info (wmi)
  "Extract a WPB:CONTEXT-INFO object from WMI, if it has one."
  (when (wpb:has-message wmi)
    (let ((msg-obj
            (extract-context-info-inner (wpb:message wmi)
                                        "extended-text-message"
                                        "image-message"
                                        "audio-message"
                                        "video-message"
                                        "document-message"
                                        "contact-message")))
      (when msg-obj
        (when (wpb:has-context-info msg-obj)
          (wpb:context-info msg-obj))))))

(defun message-quoted-contents-summary (message)
  "Returns a maximum-101-character summary of MESSAGE's quoted contents."
  (let ((qc (message-quoted-contents message)))
    (when qc
      (let ((summary (if (typep qc 'message-contents-text)
                         (contents-text qc)
                         (write-to-string qc))))
        (if (> (length summary) 100)
            (concatenate 'string (subseq summary 0 100) "…")
            summary)))))

(defun parse-message (wmi)
  "Parses WMI, a WPB:WEB-MESSAGE-INFO object, into a MESSAGE object."
  (let* ((key (wpb:key wmi))
         (key-jid (parse-jid (pb:string-value (wpb:remote-jid key))))
         (from-me (wpb:from-me key))
         (new-key (if from-me
                      (make-instance 'message-key-sending
                                     :jid key-jid)
                      (if (wpb:has-participant wmi)
                          (make-instance 'message-key-group-receiving
                                         :jid key-jid
                                         :participant (parse-jid (pb:string-value (wpb:participant wmi))))
                          (make-instance 'message-key-receiving
                                         :jid key-jid))))
         (context-info (extract-context-info wmi))
         (quoted-participant (when (and context-info (wpb:has-participant context-info))
                               (parse-jid (pb:string-value (wpb:participant context-info)))))
         (quoted-contents (when (and context-info (wpb:has-quoted-message context-info))
                            (parse-non-stub-message-contents (wpb:quoted-message context-info)))))
    (make-instance 'message
                   :id (pb:string-value (wpb:id key))
                   :ts (wpb:message-timestamp wmi)
                   :key new-key
                   :status (symbolize-message-status (wpb:status wmi))
                   :contents (parse-message-contents wmi)
                   :raw wmi
                   :quoted-participant quoted-participant
                   :quoted-contents quoted-contents)))

(defun make-web-message-info (to-jid contents)
  "Make a WPB:WEB-MESSAGE-INFO object for a WPB:MESSAGE object CONTENTS to be sent to TO-JID, containing the text TEXT.
Returns the message object as first value, and its ID as second."
  (check-type to-jid jid)
  (check-type contents wpb:message)
  (let* ((id (format nil "~X" (ironclad:random-bits (* 12 8))))
         (local-time:*default-timezone* local-time:+utc-zone+)
         (ts (local-time:timestamp-to-unix (local-time:now)))
         (key (make-instance 'wpb:message-key))
         (ret (make-instance 'wpb:web-message-info)))
    (setf (wpb:remote-jid key) (pb:string-field (jid-to-message-target to-jid)))
    (setf (wpb:from-me key) t)
    (setf (wpb:id key) (pb:string-field id))
    (setf (wpb:key ret) key)
    (setf (wpb:message ret) contents)
    (setf (wpb:message-timestamp ret) ts)
    (setf (wpb:status ret) 1) ; PENDING
    (values ret id)))

(defun make-simple-text-message (to-jid text)
  "Make a WPB:WEB-MESSAGE-INFO object for a text message to be sent to TO-JID, containing the text TEXT.
Returns the message object as first value, and its ID as second."
  (check-type text string)
  (let ((contents (make-instance 'wpb:message)))
    (setf (wpb:conversation contents) (pb:string-field text))
    (make-web-message-info to-jid contents)))

(defun make-image-message (to-jid contents)
  "Make a WPB:WEB-MESSAGE-INFO object for the MESSAGE-CONTENTS-IMAGE object CONTENTS to be sent to TO-JID.
Returns the message object as first value, and its ID as second."
  (check-type to-jid jid)
  (check-type contents message-contents-image)
  (with-slots (height-px width-px jpeg-thumbnail caption file-info)
      contents
    (with-slots (url sha256 media-key mime-type enc-sha256 length-bytes)
        file-info
      (let ((contents (make-instance 'wpb:image-message))
            (msg-obj (make-instance 'wpb:message)))
        (setf (wpb:url contents) url)
        (setf (wpb:file-sha256 contents) sha256)
        (setf (wpb:file-enc-sha256 contents) enc-sha256)
        (setf (wpb:mimetype contents) mime-type)
        (setf (wpb:file-length contents) length-bytes)
        (setf (wpb:media-key contents) media-key)
        (setf (wpb:height contents) height-px)
        (setf (wpb:width contents) width-px)
        (when caption
          (setf (wpb:caption contents) caption))
        (setf (wpb:jpeg-thumbnail contents) jpeg-thumbnail)
        (setf (wpb:url contents) url)
        (setf (wpb:image-message msg-obj) contents)
        (make-web-message-info to-jid msg-obj)))))

(defun encode-message (msg)
  "Encodes the WPB:WEB-MESSAGE-INFO object stored in MSG to binary, returning an octet vector."
  (let* ((len (pb:octet-size msg))
         (out (make-array len
                          :initial-element 0
                          :element-type '(unsigned-byte 8))))
    (pb:serialize msg out 0 len)
    out))
